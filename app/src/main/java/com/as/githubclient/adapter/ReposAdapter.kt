package com.`as`.githubclient.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.`as`.githubclient.DetailsActivity
import com.`as`.githubclient.databinding.ItemBinding
import com.`as`.githubclient.model.Repository

class ReposAdapter(private val context: Context, private val reposList: List<Repository>) :
    RecyclerView.Adapter<ReposAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)
        val binding = ItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val repo = reposList[position]
        holder.name.text = repo.name
        holder.description.text =
            if (repo.description.isNullOrBlank()) "No description" else repo.description

        holder.itemView.setOnClickListener {
            val myIntent = Intent(context, DetailsActivity::class.java).apply {
                putExtra(REPO_NAME, repo.name)
                putExtra(REPO_FORKS, repo.forks)
                putExtra(REPO_WATCHERS, repo.watchers)
                putExtra(REPO_ISSUES, repo.issues)
                if (repo.description.isNullOrBlank()) putExtra(REPO_DESCRIPTION, "No description")
                else putExtra(REPO_DESCRIPTION, "Description: ${repo.description}")
                repo.parent?.let { putExtra(REPO_PARENT, it.name) }
            }
            context.startActivity(myIntent)
        }
    }

    override fun getItemCount() = reposList.size

    inner class ViewHolder(binding: ItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val name = binding.repoName
        val description = binding.repoDescription
    }

    private companion object{
        const val REPO_NAME = "REPO_NAME"
        const val REPO_FORKS = "REPO_FORKS"
        const val REPO_WATCHERS = "REPO_WATCHERS"
        const val REPO_ISSUES = "REPO_ISSUES"
        const val REPO_DESCRIPTION = "REPO_DESCRIPTION"
        const val REPO_PARENT = "REPO_PARENT"
    }
}