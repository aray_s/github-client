package com.`as`.githubclient

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.`as`.githubclient.databinding.ActivityDetailsBinding

class DetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        with(binding) {
            val myIntent = intent.apply {
                name.text = getStringExtra(REPO_NAME)
                description.text = getStringExtra(REPO_DESCRIPTION)
                forks.text = getString(R.string.forks, getIntExtra(REPO_FORKS, -1))
                watchers.text = getString(R.string.watchers, getIntExtra(REPO_WATCHERS, -1))
                issues.text = getString(R.string.issues, getIntExtra(REPO_ISSUES, -1))
            }
            val str: String? = myIntent.getStringExtra(REPO_PARENT)
            if (str.isNullOrBlank()) parent.visibility = View.GONE
            else {
                parent.visibility = View.VISIBLE
                parent.text = "Parent: $str"
            }
        }
    }
    private companion object{
        const val REPO_NAME = "REPO_NAME"
        const val REPO_FORKS = "REPO_FORKS"
        const val REPO_WATCHERS = "REPO_WATCHERS"
        const val REPO_ISSUES = "REPO_ISSUES"
        const val REPO_DESCRIPTION = "REPO_DESCRIPTION"
        const val REPO_PARENT = "REPO_PARENT"
    }
}