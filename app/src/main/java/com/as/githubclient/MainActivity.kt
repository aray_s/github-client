package com.`as`.githubclient

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.`as`.githubclient.adapter.ReposAdapter
import com.`as`.githubclient.api.RetrofitClient
import com.`as`.githubclient.databinding.ActivityMainBinding
import com.`as`.githubclient.model.Repository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var repos: MutableList<Repository> = mutableListOf()
    private lateinit var adapter: ReposAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.recyclerView.layoutManager = LinearLayoutManager(this)

        adapter = ReposAdapter(this, repos)

        with(binding) {
            recyclerView.adapter = adapter

            searchBtn.setOnClickListener {
                if (editText.text.isNullOrBlank()) Toast.makeText(
                    baseContext,
                    "Organization name cannot be empty",
                    Toast.LENGTH_LONG
                ).show()
                else {
                    progressBar.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                    getReposList(editText.text.toString())
                }
            }
        }
    }

    private fun getReposList(orgName: String) {
        val mService = RetrofitClient.createInstance(orgName)

        mService.getReposList().enqueue(object : Callback<MutableList<Repository>> {
            override fun onFailure(call: Call<MutableList<Repository>>, t: Throwable) {
                repos.clear()
                runOnUiThread {
                    binding.progressBar.visibility = View.GONE
                    binding.recyclerView.visibility = View.GONE
                    Toast.makeText(
                        baseContext,
                        "Load failed",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onResponse(
                call: Call<MutableList<Repository>>,
                response: Response<MutableList<Repository>>
            ) {
                if (response.body() == null) runOnUiThread {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(
                        baseContext,
                        "No repos found for organization $orgName",
                        Toast.LENGTH_LONG
                    ).show()
                }
                else {
                    repos.clear()
                    repos.addAll(response.body() as MutableList<Repository>)
                    runOnUiThread {
                        adapter.notifyDataSetChanged()
                        binding.progressBar.visibility = View.GONE
                        binding.recyclerView.visibility = View.VISIBLE
                    }
                }
            }
        })
    }

}


