package com.`as`.githubclient.api

import com.`as`.githubclient.model.Repository
import retrofit2.Call
import retrofit2.http.*

interface RetrofitServices {
    @GET("repos")
    fun getReposList(): Call<MutableList<Repository>>
}