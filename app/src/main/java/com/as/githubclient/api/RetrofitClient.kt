package com.`as`.githubclient.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val BASE_URL = "https://api.github.com/orgs/"

object RetrofitClient {
    fun createInstance(orgName: String): RetrofitServices {
        val retrofit = Retrofit.Builder()
            .baseUrl("$BASE_URL$orgName/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit.create(RetrofitServices::class.java)
    }
}