package com.`as`.githubclient.model

data class Repository(
    val name: String,
    val description: String,
    val forks: Int,
    val watchers: Int,
    val issues: Int,
    val parent: Repository?
)
